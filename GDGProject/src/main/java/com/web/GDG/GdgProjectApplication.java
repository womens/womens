package com.web.GDG;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GdgProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(GdgProjectApplication.class, args);
	}

}
