package com.web.GDG.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.web.GDG.domains.Abuse;
import com.web.GDG.sevices.AbuseService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/viewReports")
public class ViewAbuseController {
	@Autowired 
	AbuseService abuseService;
	
	
	
	@GetMapping
	public String getAnnouncements(Model model) {
		List<Abuse> reports=abuseService.findAllAbuses();
		if(reports.isEmpty())
			model.addAttribute("reports","empty");
		else
			model.addAttribute("reports",reports);
		log.info("getting reports");
		return "viewReports";
	}
	@PostMapping(params="edit")
	public String editAnnouncement(@RequestParam("a_id") Long id,RedirectAttributes redirect,Model model,HttpSession session) {
		Abuse a=abuseService.findAbuse(id);
		
		
		
		return "redirect:/announcement";
	}

}
