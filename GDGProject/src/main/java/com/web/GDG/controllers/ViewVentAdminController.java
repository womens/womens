package com.web.GDG.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.web.GDG.domains.VentMessage;
import com.web.GDG.sevices.VentService;

@Controller 
@RequestMapping("/viewVents")
public class ViewVentAdminController {
	@Autowired
	VentService ventService;
	
	@GetMapping
	public String getVents(Model model) {
		model.addAttribute("vents",ventService.findVentsBasedonStatus("Pending"));
		return "viewVents";
	}
	
	@PostMapping(params="post")
	public String postVents(@RequestParam("a_id") Long id) {
		VentMessage v=ventService.findVent(id);
		v.setStatus("Posted");
		ventService.saveVent(v);
		return "redirect:/viewVents";
		
	}

}
