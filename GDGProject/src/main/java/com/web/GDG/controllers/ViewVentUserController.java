package com.web.GDG.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.web.GDG.domains.Comment;
import com.web.GDG.domains.User;
import com.web.GDG.repositories.CommentRepository;
import com.web.GDG.sevices.CommentService;
import com.web.GDG.sevices.UserService;
import com.web.GDG.sevices.VentService;

@Controller
@RequestMapping("/commentVents")
public class ViewVentUserController {
	@Autowired
	CommentService commentService;
	
	@Autowired
	VentService ventService;
	
	@Autowired
	UserService userService;
	
	@GetMapping
	public String getPage(Model model) {
		model.addAttribute("commentStudent", new Comment());
		model.addAttribute("postedVents", ventService.findVentsBasedonStatus("Posted"));
		model.addAttribute("postedComments", commentService.findComments());
		return "commentVents";
	}
	
	@PostMapping(params="post")
	public String postComment(@Valid @ModelAttribute("commentStudent")Comment c,Errors errors,@AuthenticationPrincipal UserDetails userDetails,RedirectAttributes attribute) {
		if(errors.hasErrors()) {
			return "commentVents";
		}
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		
		
		
		c.setUser(user);
		c.setVent(ventService.findVentByUser(user.getId()));
		commentService.saveComment(c);
		
		

		return "redirect:/commentVents";
	}
	
	

}
