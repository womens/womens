package com.web.GDG.controllers;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.web.GDG.domains.VentMessage;
import com.web.GDG.sevices.UserService;
import com.web.GDG.sevices.VentService;

import lombok.extern.slf4j.Slf4j;

import com.web.GDG.domains.User;

@Controller
@RequestMapping("/ventPage")
@Slf4j
public class VentController {
	@Autowired
	UserService userService;
	@Autowired
	VentService ventService;
	
	@GetMapping
	public void ventHere(Model model) {
		model.addAttribute("ventAdmin", new VentMessage());
	}
	
	@PostMapping(params="post")
	public String postAnnouncement(@Valid @ModelAttribute("ventAdmin") VentMessage msg, Errors errors,HttpSession session,@AuthenticationPrincipal UserDetails userDetails,RedirectAttributes attribute) {
		
		if(errors.hasErrors()) {
			return "ventPage";
		}
		
		User user = userService.findUserByUsername(userDetails.getUsername());
		
		
		msg.setStatus("Pending");
		msg.setUser(user);
		
		ventService.saveVent(msg);
		log.info("Dean Announcement saved");
		
		
		attribute.addFlashAttribute("success","Your vent has been sent successfully");

		return "redirect:/ventPage";
	}
	

}
