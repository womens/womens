package com.web.GDG.controllers;


import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

import com.web.GDG.domains.*;
import com.web.GDG.sevices.*;

import lombok.extern.slf4j.Slf4j;



@Controller
@Slf4j
@RequestMapping("/reportAbuse")
public class ReportController {
	
	@Autowired
	UserService userService;
	@Autowired
	AbuseService abuseService;
	
	@GetMapping
	  public String home(Model model) {
		model.addAttribute("abuse",new Abuse());
	    return "reportAbuse";    
	  }
	
	@PostMapping(params="post")
	public String updateAnnouncement(HttpSession session,@Valid @ModelAttribute("abuse") Abuse abuse,@AuthenticationPrincipal UserDetails userDetails,Errors errors,RedirectAttributes attribute) {
		if(errors.hasErrors()) {
			return "reportAbuse";
		}
		User user = userService.findUserByUsername(userDetails.getUsername());
		
			
		abuse.setUser(user);
		
		abuseService.savePost(abuse);
		log.info("Teacher Announcement saved");
		attribute.addFlashAttribute("success","Your report has been sent successfully");

		
		return "redirect:/reportAbuse";
		
	}

}
