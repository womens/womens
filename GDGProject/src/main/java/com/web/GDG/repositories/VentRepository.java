package com.web.GDG.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.web.GDG.domains.VentMessage;

public interface VentRepository extends CrudRepository<VentMessage, Long> {
	@Query(value="SELECT * FROM messages where messages.status=:status",nativeQuery=true)
	List<VentMessage> findVentsByStatus(String status);
	
	
}
