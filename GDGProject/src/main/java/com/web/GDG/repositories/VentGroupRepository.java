package com.web.GDG.repositories;



import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.web.GDG.domains.Vent;

public interface VentGroupRepository extends CrudRepository<Vent, Long> {
	@Query(value="SELECT * FROM vents_users where users_id=:id",nativeQuery=true)
	Long findVentsByUser(Long id);
}
