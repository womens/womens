package com.web.GDG.repositories;

import org.springframework.data.repository.CrudRepository;

import com.web.GDG.domains.Comment;

public interface CommentRepository extends CrudRepository<Comment, Long> {

}
