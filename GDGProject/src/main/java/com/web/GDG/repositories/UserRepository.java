package com.web.GDG.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.web.GDG.domains.User;

public interface UserRepository extends CrudRepository<User, Long> {
	@Query(value="SELECT * FROM users where users.username=:user_name",nativeQuery=true)
	User find_By_Username(String user_name);
	
	@Query(value="SELECT * FROM users join roles ON users.id=roles.user_id where roles.user_role  =:role",nativeQuery=true)
	List<User> findAllByRole(String role);
}
