package com.web.GDG.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import com.web.GDG.domains.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {
	
	@Query(value="SELECT * FROM roles where roles.user_role=:role",nativeQuery=true)
	Role findByuserRole(String role);
}
