package com.web.GDG.repositories;

import org.springframework.data.repository.CrudRepository;

import com.web.GDG.domains.*;

public interface AbuseRepository extends CrudRepository<Abuse, Long> {

}
