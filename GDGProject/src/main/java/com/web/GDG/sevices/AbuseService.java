package com.web.GDG.sevices;

import java.util.List;

import com.web.GDG.domains.*;

public interface AbuseService{
	void savePost(Abuse p);
	List<Abuse> findAllAbuses();
	Abuse findAbuse(Long id);
}
