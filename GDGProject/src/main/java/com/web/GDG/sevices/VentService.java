package com.web.GDG.sevices;

import java.util.List;

import com.web.GDG.domains.Vent;
import com.web.GDG.domains.VentMessage;

public interface VentService {
	void saveVent(VentMessage msg);
	List<VentMessage> findVentsBasedonStatus(String status);
	VentMessage findVent(Long id);
	Vent findVentByUser(Long id);
}
