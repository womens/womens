package com.web.GDG.sevices;

import java.util.List;

import com.web.GDG.domains.Comment;

public interface CommentService {
	void saveComment(Comment c);
	List<Comment> findComments();

}
