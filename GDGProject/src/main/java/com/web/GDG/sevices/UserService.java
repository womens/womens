package com.web.GDG.sevices;


import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;


import com.web.GDG.domains.User;


public interface UserService extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user);
	void saveTeacher(User user);
	List<User> findAllUsersByRole(String role);
	void changePassword(User user, String password);
	
	
}