package com.web.GDG.sevices;



import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.web.GDG.domains.Role;
import com.web.GDG.domains.User;
import com.web.GDG.repositories.RoleRepository;
import com.web.GDG.repositories.UserRepository;



@Service
public class UserServiceImpl implements UserService{
	
	
	
	private UserRepository userRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,RoleRepository roleRepository,BCryptPasswordEncoder bCryptPasswordEncoder) {
    	this.roleRepository=roleRepository;
    	this.userRepository=userRepository;
    	this.bCryptPasswordEncoder=bCryptPasswordEncoder;
    }
   

    public User findUserByUsername(String username) {
    	return userRepository.find_By_Username(username);
    }
    public void saveTeacher(User user) {
    	Role r;
    	r=new Role();
    	r.setUser(user);
    	r.setUserRole("TEACHER");
    	user.setRole(r);
    	user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
    	userRepository.save(user);
    	roleRepository.save(r);
    	
    }
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
    }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		User user = userRepository.find_By_Username(username);
		
		if(user != null) {
			return user;
		}
		throw new UsernameNotFoundException("User '" + username + "' not found");
	}

	

	@Override
	public List<User> findAllUsersByRole(String role) {
		List<User> users=userRepository.findAllByRole(role);
		return users;
	}

	@Override
	public void changePassword(User user, String password) {
		
	    user.setPassword(bCryptPasswordEncoder.encode(password));
	    userRepository.save(user);
	}
}
