package com.web.GDG.sevices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.GDG.domains.Vent;
import com.web.GDG.domains.VentMessage;
import com.web.GDG.repositories.VentGroupRepository;
import com.web.GDG.repositories.VentRepository;

@Service
public class VentServiceImpl implements VentService {
	@Autowired
	VentRepository ventRepository;
	@Autowired
	VentGroupRepository groupRepository;
	@Override
	public void saveVent(VentMessage msg) {
		ventRepository.save(msg);
		
	}

	@Override
	public List<VentMessage> findVentsBasedonStatus(String status) {
		// TODO Auto-generated method stub
		return ventRepository.findVentsByStatus(status);
	}

	@Override
	public VentMessage findVent(Long id) {
		// TODO Auto-generated method stub
		return ventRepository.findById(id).get();
	}

	@Override
	public Vent findVentByUser(Long id) {
		// TODO Auto-generated method stub
		Long v_id = groupRepository.findVentsByUser(id);
		return groupRepository.findById(v_id).get();
	}

}
