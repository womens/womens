package com.web.GDG.sevices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.GDG.domains.Abuse;
import com.web.GDG.repositories.AbuseRepository;

@Service
public class AbuseServiceImpl implements AbuseService{

	@Autowired
	AbuseRepository abuseRepository;
	
	@Override
	public void savePost(Abuse abuse) {
		abuseRepository.save(abuse);
	}

	@Override
	public List<Abuse> findAllAbuses() {
		return (List<Abuse>)abuseRepository.findAll();
	}

	@Override
	public Abuse findAbuse(Long id) {
		
		return abuseRepository.findById(id).get();
	}

}
