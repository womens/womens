package com.web.GDG.sevices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.GDG.domains.Comment;
import com.web.GDG.repositories.CommentRepository;

@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	CommentRepository commentRepository;
	@Override
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
		
	}

	@Override
	public List<Comment> findComments() {
		// TODO Auto-generated method stub
		return (List<Comment>)commentRepository.findAll();
	}

}
