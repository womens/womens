package com.web.GDG.domains;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;

import com.web.GDG.sevices.UserService;

import lombok.Data;

@Entity
@Data
@Table(name="vents") 
public class Vent {
	
	
	@ManyToMany
	private Set<User> users = new HashSet<User>();
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="name")
	@NotNull(message="Please assign a name")
	private String name;
	
	
	
	
	
	
	

}
