package com.web.GDG.domains;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;


@Entity
@Data
@Table(name="messages")
public class VentMessage {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@Column(name="created_at")
	private Date createdAt;
	
	@PrePersist
	void placedAt() {
		this.createdAt = new Date();
	}	
	
	@Column(name="ventmsg")
	@NotNull(message="Please assign a role")
	private String ventMessage;
	
	@JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="student_id")
	private User user;
	
	@Column(name="status")
	private String status;
	
}
