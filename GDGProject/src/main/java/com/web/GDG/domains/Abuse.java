package com.web.GDG.domains;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.web.GDG.domains.User;

import lombok.Data;

@Data
@Entity
@Table(name = "abuses")
public class Abuse {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="created_at")
	private Date createdAt;
	
	@PrePersist
	void placedAt() {
		this.createdAt = new Date();
	}	
	
	@DateTimeFormat (pattern="dd-MMM-YYYY")
    private String abuseDate;
	
	@Column(name = "type")
    @NotEmpty(message = "Please provide the abuse type")
    @NotNull
    private String abuseType;
    
    @Column(name = "description")
    @NotEmpty(message = "Please provide the description")
    @NotNull
    private String description;
    
    @Column(name = "abuser")
    @NotEmpty(message = "Please provide the abuser's name")
    @NotNull
    private String abuser;
    
    @JsonIgnore
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="student_id")
	private User user;
}
