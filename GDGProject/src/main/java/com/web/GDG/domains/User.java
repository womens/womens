package com.web.GDG.domains;

import java.util.Arrays;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;
import lombok.NoArgsConstructor;

import com.web.GDG.domains.Role;

@Data
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User implements UserDetails {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
   
	
    @NotBlank(message = "Please provide a username")
    private String username;
   
    @Size(min = 8, message = "Your password must have at least 8 characters")
    @NotBlank(message = "Please provide your password")
    @NotNull(message="Password doesn't match")
    private String password; 
   
    @Column(name = "first_name")
    @NotBlank(message = "Please provide your first name")
    private String firstName;
    
    @Column(name = "last_name")
    @NotBlank(message = "Please provide your last name")
    private String lastName;
    
    @Email(message="Invalid Email")
    @NotBlank(message = "Please provide your email")
    private String email;
    
    @OneToOne(
            cascade =  CascadeType.ALL,
            mappedBy = "user")
    private Role role;
    
    
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		return Arrays.asList(new SimpleGrantedAuthority(role.getUserRole()));
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


}
