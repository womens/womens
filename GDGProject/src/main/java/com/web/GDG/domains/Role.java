package com.web.GDG.domains;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.web.GDG.domains.User;
import lombok.Data;

@Data
@Entity
@Table(name = "roles")
public class Role {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@JsonIgnore
	@OneToOne(cascade =  CascadeType.ALL, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
	private User user;
    
	@Column(name="user_role")
	@NotNull(message="Please assign a role")
    private String userRole;
}
