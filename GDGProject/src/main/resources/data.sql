INSERT INTO users(username,password) VALUES ('admin','$2a$10$10q7QTIrVJCZUxbZMVrJ6uL/FuPiEG.Jq8upuyAr8Aa6w9oevdTAm');
SET @id := (SELECT id FROM users);
INSERT INTO roles(user_role,user_id) VALUES ('ADMIN',@id);

INSERT INTO vents(name) VALUES ("Vent Here");

INSERT INTO users(username,password,`first_name`,`last_name`,email) VALUES ('ATR/6676/09','$2a$10$10q7QTIrVJCZUxbZMVrJ6uL/FuPiEG.Jq8upuyAr8Aa6w9oevdTAm','Meron','Zerihun','merry393zerihun@gmail.com');
SET @id := (SELECT id FROM users where username='ATR/6676/09');
INSERT INTO roles(user_role,user_id) VALUES ('STUDENT',@id);
INSERT INTO vents_users (vent_id,users_id) VALUES(1, @id);

INSERT INTO users(username,password,`first_name`,`last_name`,email) VALUES ('ATR/3308/09','$2a$10$10q7QTIrVJCZUxbZMVrJ6uL/FuPiEG.Jq8upuyAr8Aa6w9oevdTAm','Meskerem','Zelalem','meskeremzelalem@gmail.com');
SET @id := (SELECT id FROM users where username='ATR/3308/09');
INSERT INTO roles(user_role,user_id) VALUES ('STUDENT',@id);
INSERT INTO vents_users (vent_id,users_id) VALUES(1, @id);


